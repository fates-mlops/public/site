#-----------------------------------------------------
# Some usefull instructions...
#
DIAGRAMS        = images/*.svg  
DEP				= *.adoc 
ANTORA			= npx antora
DOCS			= ./public
#-----------------------------------------------------


all: $(DOCS)/index.html
	antora --fetch antora-playbook.yml

# npm i -D -E antora
deploy: $(DOCS)/index.html
	@echo "========================================"
	@echo "==> Deploy updates "
	./deploy